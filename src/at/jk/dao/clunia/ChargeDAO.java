package at.jk.dao.clunia;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;



public class ChargeDAO {
	public List<String> getAllChargeByMemberID(int id){
		List<String> chargen = new ArrayList<String>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();

			ResultSet resultSet = stmt.executeQuery("select * from MemberChargen");
			
/*
 * create view MemberChargen as
select Member.id, Member.name, Member.lastname, Member.vulgo, Member.imageID, Member.birthDate, Charge.name as chargenname from Member_Charge 
	inner join Member
		on Member.id = Member_Charge.member_id
	inner join Charge
		on Charge.id = Member_Charge.charge_id;
 * 
 * 
 */
	
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				if (id == resultSet.getInt("memberid")){
					chargen.add(resultSet.getString("chargenname"));
				}
				
				resultSet.next();
			}
			
			return chargen;
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return chargen;
	}
	
	
		
}
