package at.jk.dao.clunia;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
 
import at.jk.vo.clunia.MemberVO;

public class MemberDAO {
	
	
	public List<MemberVO> getAllMembers(){
		List<MemberVO> members = new ArrayList<MemberVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from BigMember order by vulgo");
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				//public MemberVO(int id, String name, String lastname, String vulgo, String imageID, String birthDate, String state,
				//String street, int number, String place, int zipcode, String degree, String password, List<String> chargen ) {
				MemberVO member = new MemberVO(resultSet.getInt("id"),
									resultSet.getString("username"),
									resultSet.getString("lastname"),
									resultSet.getString("vulgo"),
									resultSet.getString("imageID"),
									resultSet.getString("birthDate"),
									resultSet.getString("state"),
									resultSet.getString("street"),
									resultSet.getString("number"),
									resultSet.getString("place"),
									resultSet.getString("zipcode"),
									resultSet.getString("degree"),
									null, //password
									new ChargeDAO().getAllChargeByMemberID(resultSet.getInt("id")));
				
				members.add(member);
				resultSet.next();
			}
			
			return members;
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return members;
	}
	
	public List<MemberVO> getAllChargen(){
		List<MemberVO> members = new ArrayList<MemberVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from BigMember order by vulgo");
			
			ChargeDAO chargedao = new ChargeDAO();
			
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				if (!(chargedao.getAllChargeByMemberID(resultSet.getInt("id")).isEmpty())){
					MemberVO member = new MemberVO(resultSet.getInt("id"),
										resultSet.getString("username"),
										resultSet.getString("lastname"),
										resultSet.getString("vulgo"),
										resultSet.getString("imageID"),
										resultSet.getString("birthDate"),
										resultSet.getString("state"),
										resultSet.getString("street"),
										resultSet.getString("number"),
										resultSet.getString("place"),
										resultSet.getString("zipcode"),
										resultSet.getString("degree"),
										null,
										chargedao.getAllChargeByMemberID(resultSet.getInt("id")));
					members.add(member);
				}
				resultSet.next();
			}
			
			return members;
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return members;
	}
	
	public List<MemberVO> getAllAktiveMember(){
		List<MemberVO> members = new ArrayList<MemberVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from BigMember order by vulgo");
			
			ChargeDAO chargedao = new ChargeDAO();
			
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				if (!(resultSet.getString("state").equals("Philister"))){
					MemberVO member = new MemberVO(resultSet.getInt("id"),
										resultSet.getString("username"),
										resultSet.getString("lastname"),
										resultSet.getString("vulgo"),
										resultSet.getString("imageID"),
										resultSet.getString("birthDate"),
										resultSet.getString("state"),
										resultSet.getString("street"),
										resultSet.getString("number"),
										resultSet.getString("place"),
										resultSet.getString("zipcode"),
										resultSet.getString("degree"),
										null,
										chargedao.getAllChargeByMemberID(resultSet.getInt("id")));
					members.add(member);
				}
				resultSet.next();
			}
			
			return members;
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return members;
	}
	
	/*if therer ar more states the value in the if have to bee changed*/
	public List<MemberVO> getAllAktiveChargen(){
		List<MemberVO> members = new ArrayList<MemberVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from BigMember order by vulgo");
			
			ChargeDAO chargedao = new ChargeDAO();
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				if ((!(chargedao.getAllChargeByMemberID(resultSet.getInt("id")).isEmpty())) && !(resultSet.getString("state").equals("Philister"))){
					MemberVO member = new MemberVO(resultSet.getInt("id"),
										resultSet.getString("username"),
										resultSet.getString("lastname"),
										resultSet.getString("vulgo"),
										resultSet.getString("imageID"),
										resultSet.getString("birthDate"),
										resultSet.getString("state"),
										resultSet.getString("street"),
										resultSet.getString("number"),
										resultSet.getString("place"),
										resultSet.getString("zipcode"),
										resultSet.getString("degree"),
										null,
										chargedao.getAllChargeByMemberID(resultSet.getInt("id")));
					members.add(member);
				
				}
				resultSet.next();
			}
			
			return members; 
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return members;
	}
	
	public List<MemberVO> getAllPhilisterChargen(){
		List<MemberVO> members = new ArrayList<MemberVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from BigMember order by vulgo");
			
			ChargeDAO chargedao = new ChargeDAO();
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				if ((!(chargedao.getAllChargeByMemberID(resultSet.getInt("id")).isEmpty())) && resultSet.getString("state").equals("Philister") ){
					MemberVO member = new MemberVO(resultSet.getInt("id"),
										resultSet.getString("username"),
										resultSet.getString("lastname"),
										resultSet.getString("vulgo"),
										resultSet.getString("imageID"),
										resultSet.getString("birthDate"),
										resultSet.getString("state"),
										resultSet.getString("street"),
										resultSet.getString("number"),
										resultSet.getString("place"),
										resultSet.getString("zipcode"),
										resultSet.getString("degree"),
										null,
										chargedao.getAllChargeByMemberID(resultSet.getInt("id")));
					members.add(member);
				}
				resultSet.next();
			}
			
			return members; 
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return members;
	}

	public List<MemberVO> getAllPhilisterMember() {
		List<MemberVO> members = new ArrayList<MemberVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from BigMember order by vulgo");
			
			ChargeDAO chargedao = new ChargeDAO();
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				if (resultSet.getString("state").equals("Philister")){
					MemberVO member = new MemberVO(resultSet.getInt("id"),
										resultSet.getString("username"),
										resultSet.getString("lastname"),
										resultSet.getString("vulgo"),
										resultSet.getString("imageID"),
										resultSet.getString("birthDate"),
										resultSet.getString("state"),
										resultSet.getString("street"),
										resultSet.getString("number"),
										resultSet.getString("place"),
										resultSet.getString("zipcode"),
										resultSet.getString("degree"),
										null,
										chargedao.getAllChargeByMemberID(resultSet.getInt("id")));
					members.add(member);
				}
				resultSet.next();
			}
			
			return members; 
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return members;
	}
	
	public Boolean checkIfMemberByVulgoAndPasswordExists(String vulgo, String password){
		Connection connection;
		boolean state = false;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
			
			if (!(vulgo.contains(";")|| password.contains(";"))){
				ResultSet resultSet = stmt.executeQuery("select * from BigMember where vulgo = '"+vulgo+"' and BigMember.password = '"+password+"';");
			
			
				resultSet.first();
				if (resultSet.absolute(1)){
					while(!(resultSet.isAfterLast())){	
						state = true;
										
						resultSet.next();
						
					}
				}
			}
			
			
		}catch (ClassNotFoundException e){
			return false;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		return state;
		
	}
	public boolean addMember(MemberVO member, String vulgo, String password){
		Connection connection;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
			if (this.checkIfMemberByVulgoAndPasswordExists(vulgo, password)){
				stmt.executeUpdate("insert into Member (name, lastname, vulgo, imageID, birthDate, state_id ,degree, password,street, place, number, zipcode) values ('"
			+member.getName()+"','"+member.getLastname()+"','"+member.getVulgo()+"', '"+member.getImageID()+"' , '"+member.getBirthDate()+"', "+member.getState()+", '"+member.getDegree()+"','"+member.getPassword()+"','"+member.getStreet()+"','"+member.getPlace()+"'," +member.getNumber()+","+member.getZipcode()+");");
			}
			return true;
		}catch (ClassNotFoundException e){
			return false;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		
		
	}
	
}
