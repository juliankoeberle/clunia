package at.jk.dao.clunia;


import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import at.jk.vo.clunia.*;

public class EventDAO {
	
	public List<EventVO> getAllEvents(){
		List<EventVO> events = new ArrayList<EventVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("select * from Event order by date");
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){
				EventVO event = new EventVO(resultSet.getInt("id"),
									resultSet.getString("name"),
									resultSet.getString("text"),
									resultSet.getString("date"),
									resultSet.getString("imageID"),
									resultSet.getString("location"));
				
				events.add(event);
				resultSet.next();
			}
			
			return events;
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return events;
	}
	
	public List<EventVO> getEventByDate(String date){
		List<EventVO> events = new ArrayList<EventVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
			
			if (date.contains(";") || date.contains("+") || date.contains("/")){
				return null;
			}
			else{
				System.out.println(date);
				ResultSet resultSet = stmt.executeQuery("select * from Event where date(date) ='" + date+"'");
				
				resultSet.first();
				while(!(resultSet.isAfterLast())){
					EventVO event = new EventVO(resultSet.getInt("id"),
										resultSet.getString("name"),
										resultSet.getString("text"),
										resultSet.getString("date"),
										resultSet.getString("imageID"),
										resultSet.getString("location"));
					
					events.add(event);
					resultSet.next();
				}
				
				return events;
			}
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return events;
		
	}
	public void addEvents(EventVO eventVO){
		Connection connection;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
			
			String insert = "insert into Event (name, text, date, imageID, location) values ("+"'"+eventVO.getName()+"'"+", "+"'"+eventVO.getText()+"'"+", "+"'"+eventVO.getDate()+"'"+", "+"'"+eventVO.getImageID()+"'"+", '"+eventVO.getLocation()+"');";
			stmt.executeUpdate(insert);
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e){
			e.printStackTrace();
		}
		
			
		
		
		
		
	}
	
	
	
	
}
