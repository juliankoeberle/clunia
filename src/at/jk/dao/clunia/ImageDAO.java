package at.jk.dao.clunia;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import at.jk.vo.clunia.ImageVO;

public class ImageDAO {
	public List<ImageVO> getAllImagesByReportId(int id){
		List<ImageVO> images = new ArrayList<ImageVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from Image");
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				if (id == resultSet.getInt("reportId")){
					ImageVO imageVO = new ImageVO(resultSet.getInt("id"),
													resultSet.getString("description"),
													resultSet.getString("imageId"));
					images.add(imageVO);
					
				}
				
				
				resultSet.next();
			}
			
			return images; 
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return images;
	}
}
