package at.jk.dao.clunia;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import at.jk.vo.clunia.CLUnierVO;

public class CLUnierDAO {
	public List<CLUnierVO> getAllCLUnier(){
		List<CLUnierVO> clunier = new ArrayList<CLUnierVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();

			ResultSet resultSet = stmt.executeQuery("select * from CLUnier");
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				CLUnierVO clunierVO = new CLUnierVO(resultSet.getInt("id"),
													resultSet.getString("description"),
													resultSet.getString("imageID"),
													resultSet.getString("pdfID"),
													resultSet.getString("date"));
				
				resultSet.next();
				clunier.add(clunierVO);
			}
			
			return clunier;
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return clunier;
	}
	
	
	public List<CLUnierVO> getClUnierByDate(String date){
		List<CLUnierVO> clunier = new ArrayList<CLUnierVO>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
			
			if (date.contains(";") || date.contains("+") || date.contains("/")){
				return null;
			}
			else{

				ResultSet resultSet = stmt.executeQuery("select * from CLUnier where date(date) ='" + date+"'");
				
				resultSet.first();
				while(!(resultSet.isAfterLast())){	
					
					CLUnierVO clunierVO = new CLUnierVO(resultSet.getInt("id"),
														resultSet.getString("description"),
														resultSet.getString("imageID"),
														resultSet.getString("pdfID"),
														resultSet.getString("date"));
					
					resultSet.next();
					clunier.add(clunierVO);
				}
				
				return clunier;
			}
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return clunier;
		
	}
	
	
}
