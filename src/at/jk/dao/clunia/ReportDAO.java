package at.jk.dao.clunia;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import at.jk.vo.clunia.ReportVO;

public class ReportDAO {
	public List<ReportVO> getAllReportsData(){
		List<ReportVO> reports = new ArrayList<>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from Report");
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
			
				ReportVO reportVO = new ReportVO(resultSet.getInt("id"),
												resultSet.getString("title"),
												resultSet.getString("text"),
												resultSet.getString("date"),
												new ImageDAO().getAllImagesByReportId(resultSet.getInt("id")));
			
				reports.add(reportVO);
				

				resultSet.next();
			}
			
			return reports; 
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return reports;
	}
	
	public List<ReportVO> getReportById(int id){
		List<ReportVO> reports = new ArrayList<>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from Report");
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				
				if (id == resultSet.getInt("id")){
					ReportVO reportVO = new ReportVO(resultSet.getInt("id"),
													resultSet.getString("title"),
													resultSet.getString("text"),
													resultSet.getString("date"),
													new ImageDAO().getAllImagesByReportId(resultSet.getInt("id")));
				
					reports.add(reportVO);
				}
					

				resultSet.next();
			}
			
			return reports; 
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return reports;
	}
	
	public List<ReportVO> getAllReports(){
		List<ReportVO> reports = new ArrayList<>();
		Connection connection;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection(StaticValues.databaseURL);
			Statement stmt = (Statement) connection.createStatement();
		
			ResultSet resultSet = stmt.executeQuery("select * from Report");
			
			resultSet.first();
			while(!(resultSet.isAfterLast())){	
				ReportVO reportVO = new ReportVO(resultSet.getInt("id"),
													resultSet.getString("title"),
													null,
													resultSet.getString("date"),
													null);
				
				reports.add(reportVO);
				resultSet.next();
			}
					
			return reports; 
			
		}catch (ClassNotFoundException e){
			return null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return reports;
	}
	

}
