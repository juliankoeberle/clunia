package at.jk.services.clunia;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.jk.dao.clunia.ReportDAO;
import at.jk.vo.clunia.ReportVO;

@Path("/reports")
public class ReportService {
	
	//Todo Post
	//Pic 
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReports(){
		List<ReportVO> matched;
		GenericEntity<List<ReportVO>> entity;
		matched = new ReportDAO().getAllReports();
		entity = new GenericEntity<List<ReportVO>>(matched){}; 
		return Response.ok(entity).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getReportById(@PathParam ("id") int id){
		
		List<ReportVO> matched;
		GenericEntity<List<ReportVO>> entity;
		matched = new ReportDAO().getReportById(id);
		entity = new GenericEntity<List<ReportVO>>(matched){}; 
		
		Response response = Response.ok(entity).build();
		if (response == null){
			return Response.status(404).build();
		}
		else{
			return Response.ok(entity).build();
		}
	
	}
	
	
}
