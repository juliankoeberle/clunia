package at.jk.services.clunia;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import at.jk.dao.clunia.EventDAO;
import at.jk.dao.clunia.MemberDAO;
import at.jk.vo.clunia.EventVO;
import at.jk.vo.clunia.MemberVO;

@Path("/members")
public class MemberService {
	
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMembers(){
		List<MemberVO> matched;
		GenericEntity<List<MemberVO>> entity;
		matched = new MemberDAO().getAllMembers();
		entity = new GenericEntity<List<MemberVO>>(matched){}; 
		if (matched==null){
			return Response.noContent().build();
		}
		else{
			return Response.ok(entity).build();
		}
	}
	
	
	@GET
	@Path("/aktive")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAktiveMembers(){
		List<MemberVO> matched;
		GenericEntity<List<MemberVO>> entity;
		matched = new MemberDAO().getAllAktiveMember();
		entity = new GenericEntity<List<MemberVO>>(matched){}; 
		if (matched==null){
			return Response.noContent().build();
		}
		else{
			return Response.ok(entity).build();
		}
	}
	
	@GET
	@Path("/philister")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPhilisterMembers(){
		List<MemberVO> matched;
		GenericEntity<List<MemberVO>> entity;
		matched = new MemberDAO().getAllPhilisterMember();
		entity = new GenericEntity<List<MemberVO>>(matched){}; 
		if (matched==null){
			return Response.noContent().build();
		}
		else{
			return Response.ok(entity).build();
		}
	}
	
	
	
	@GET
	@Path("/chargen")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChargen(){
		
		List<MemberVO> matched;
		GenericEntity<List<MemberVO>> entity;
		matched = new MemberDAO().getAllChargen();
		entity = new GenericEntity<List<MemberVO>>(matched){}; 
		if (matched==null){
			return Response.noContent().build();
		}
		else{
			return Response.ok(entity).build();
		}
		
	}
	
	@GET
	@Path("/aktive/chargen")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAktiveChargen(){
		
		List<MemberVO> matched;
		GenericEntity<List<MemberVO>> entity;
		matched = new MemberDAO().getAllAktiveChargen();
		entity = new GenericEntity<List<MemberVO>>(matched){}; 
		if (matched==null){
			return Response.noContent().build();
		}
		else{
			return Response.ok(entity).build();
		}
		
	}
	
	@GET
	@Path("/philister/chargen")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPhilisterChargen(){
		
		List<MemberVO> matched;
		GenericEntity<List<MemberVO>> entity;
		matched = new MemberDAO().getAllPhilisterChargen();
		entity = new GenericEntity<List<MemberVO>>(matched){}; 
		if (matched==null){
			return Response.noContent().build();
		}
		else{
			return Response.ok(entity).build();
		}
		
		
	}
	
	@GET
	@Path("/checkIfUserIsInDataBase")
	@Produces({MediaType.APPLICATION_JSON})
	public Response checkIfUserIsInDataBase(
			@HeaderParam("vulgo") String vulgo,
			@HeaderParam("password") String password){
		
		try{
			if (new MemberDAO().checkIfMemberByVulgoAndPasswordExists(vulgo, password)){
				return Response.status(200).build();
			}
			else{
				return Response.status(401).build();
			}
			
		}catch(Exception e){
			return Response.status(401).build();
		}
		
	}
	

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMemberByID(){
		/*
		List<MemberVO> matched;
		GenericEntity<List<MemberVO>> entity;
		matched = new MemberDAO().getAllEvents();
		entity = new GenericEntity<List<MemberVO>>(matched){}; 
		
		return Response.ok(entity).build();
		*/
		return null;
	}
	
	//Todo POst request with authenification
	
	@POST
	@Path("/members")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response addEvent(
			@HeaderParam("vulgo") String vulgo,
			@HeaderParam("password") String password,
			MemberVO member){
		
		if ((new MemberDAO().checkIfMemberByVulgoAndPasswordExists(vulgo, password))){
			new MemberDAO().addMember(member, vulgo , password);
			String matched = "added new user";
			GenericEntity<String> entity;
			entity = new GenericEntity<String>(matched){}; 
			return Response.ok(entity).build();
		}
		
		return Response.status(401).build();
	
	}
	
}
