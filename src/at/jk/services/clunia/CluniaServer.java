package at.jk.services.clunia;

import java.io.IOException;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;

public class CluniaServer {
	public static void main(String[] args)throws IllegalArgumentException, IOException{
		HttpServer server = HttpServerFactory.create("http://localhost:8080");
		server.start();
	}
}
