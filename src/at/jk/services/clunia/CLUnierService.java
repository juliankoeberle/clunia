package at.jk.services.clunia;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.jk.dao.clunia.CLUnierDAO;
import at.jk.vo.clunia.CLUnierVO;

@Path("/clunier")
public class CLUnierService {
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCLUnier(){
		List<CLUnierVO> matched;
		GenericEntity<List<CLUnierVO>> entity;
		matched = new CLUnierDAO().getAllCLUnier();
		entity = new GenericEntity<List<CLUnierVO>>(matched){}; 
		return Response.ok(entity).build();
	}
	
	
	@GET
	@Path("/{date}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getReportById(@PathParam ("date") String date){
		
		List<CLUnierVO> matched;
		GenericEntity<List<CLUnierVO>> entity;
		matched = new CLUnierDAO().getClUnierByDate(date);
		entity = new GenericEntity<List<CLUnierVO>>(matched){}; 
		
		Response response = Response.ok(entity).build();
		if (response == null){
			return Response.status(404).build();
		}
		else{
			return Response.ok(entity).build();
		}
	
	}
	
	
	
}
