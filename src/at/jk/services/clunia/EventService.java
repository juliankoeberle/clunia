package at.jk.services.clunia;


import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import at.jk.dao.clunia.EventDAO;
import at.jk.dao.clunia.MemberDAO;
import at.jk.vo.clunia.*;

@Path("/events")
public class EventService {
	
	@GET 
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEvents(){
		List<EventVO> matched;
		GenericEntity<List<EventVO>> entity;
		matched = new EventDAO().getAllEvents();
		entity = new GenericEntity<List<EventVO>>(matched){}; 
		if (matched==null){
			return Response.noContent().build();
		}
		else{
			return Response.ok(entity).build();
		}
	}
	
	@GET
	@Path("/{date}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response getReportById(@PathParam ("date") String date){
		
		List<EventVO> matched;
		GenericEntity<List<EventVO>> entity;
		matched = new EventDAO().getEventByDate(date);
		entity = new GenericEntity<List<EventVO>>(matched){}; 
		
		Response response = Response.ok(entity).build();
		if (response == null){
			return Response.status(404).build();
		}
		else{
			return Response.ok(entity).build();
		}
	
	}
	//Todo authentifiction encoding
	@POST
	@Path("")
	@Consumes({MediaType.APPLICATION_JSON})
	//Cookie is the vulgo passsword
	public Response addEvent(
			@HeaderParam("Cookie") String cookie,
			@QueryParam("name") String name,
			@QueryParam("text") String text,
			@QueryParam("date") String date,
			@QueryParam("imgID") String imgID,
			@QueryParam("location")String location,
			@QueryParam("vulgo") String vulgo,
			@QueryParam("password") String password){
		
		if ((new MemberDAO().checkIfMemberByVulgoAndPasswordExists(vulgo, password))){
			EventVO event = new EventVO();
			event.setDate(date);
			event.setName(name);
			event.setText(text);
			event.setImageID(imgID);
			event.setLocation(location);
			
			new EventDAO().addEvents(event);
			String matched = "From Server! successfully added these elements : "
			+" name " + name 
			+" text " + event.getText()
			+" date " + event.getDate()
			+" imgID " + event.getImageID()
			+" location" + event.getLocation();
			GenericEntity<String> entity;
			entity = new GenericEntity<String>(matched){}; 
			return Response.ok(entity).build();
		}
		
		return Response.status(401).build();
	
	}
}
