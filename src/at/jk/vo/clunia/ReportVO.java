package at.jk.vo.clunia;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReportVO {
	private int id;
	private String title;
	private String text;
	private String date;
	private List<ImageVO> imageList = new ArrayList<>();
	
	public ReportVO(int id, String title, String text, String date, List<ImageVO> imageList) {
		super();
		this.id = id;
		this.title = title;
		this.text = text;
		this.date = date;
		this.imageList = imageList;
	}

	public ReportVO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<ImageVO> getImageList() {
		return imageList;
	}

	public void setImageList(List<ImageVO> imageList) {
		this.imageList = imageList;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
}
 