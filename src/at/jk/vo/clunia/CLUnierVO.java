package at.jk.vo.clunia;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CLUnierVO {
	private int id;
	private String description;
	private String imageID;
	private String pdfID;
	private String date;
	
	public CLUnierVO(){
		
	}

	public CLUnierVO(int id, String description, String imageID, String pdfID, String date) {
		super();
		this.id = id;
		this.description = description;
		this.imageID = imageID;
		this.pdfID = pdfID;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageID() {
		return imageID;
	}

	public void setImageID(String imageID) {
		this.imageID = imageID;
	}

	public String getPdfID() {
		return pdfID;
	}

	public void setPdfID(String pdfID) {
		this.pdfID = pdfID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	

	
	
	
	
	
}
