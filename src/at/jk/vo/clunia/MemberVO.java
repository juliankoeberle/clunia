package at.jk.vo.clunia;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MemberVO {
	private int id;
	private String name;
	private String lastname;
	private String vulgo;
	private String imageID;
	private String birthDate;
	private String state;
	private String degree;
	private String password;
	private List<String> chargen;
	
	private String street;
	private String number;
	private String place;
	private String zipcode;
	
	public MemberVO(int id, String name, String lastname, String vulgo, String imageID, String birthDate, String state,
			String street, String number, String place, String zipcode, String degree, String password, List<String> chargen ) {
		super();
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.vulgo = vulgo;
		this.imageID = imageID;
		this.birthDate = birthDate;
		this.state = state;
		this.degree = degree;
		this.password = password;
		this.chargen = chargen;
		
		this.street = street;
		this.number = number;
		this.place = place;
		this.zipcode = zipcode;
	}
	

	public MemberVO() {
		super();
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getVulgo() {
		return vulgo;
	}

	public void setVulgo(String vulgo) {
		this.vulgo = vulgo;
	}

	public String getImageID() {
		return imageID;
	}

	public void setImageID(String imageID) {
		this.imageID = imageID;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getChargen() {
		return chargen;
	}

	public void setChargen(List<String> chargen) {
		this.chargen = chargen;
	}

	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	public String getPlace() {
		return place;
	}


	public void setPlace(String place) {
		this.place = place;
	}


	public String getZipcode() {
		return zipcode;
	}


	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
