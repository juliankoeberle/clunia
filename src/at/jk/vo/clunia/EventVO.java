package at.jk.vo.clunia;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EventVO {
	private int id ;
	private String name;
	private String text;
	private String date;
	private String imgID;
	private String location;
	
	public EventVO() {
		super();
	}
	
	public EventVO(int id, String name, String text, String date, String imgID, String location) {
		super();
		this.id = id;
		this.name = name;
		this.text = text;
		this.date = date;
		this.imgID = imgID;
		this.location = location;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String datum) {
		this.date = datum;
	}
	public String getImageID() {
		return imgID;
	}
	public void setImageID(String imgID) {
		this.imgID = imgID;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
	
	
	
	
}


