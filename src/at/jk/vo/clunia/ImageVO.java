package at.jk.vo.clunia;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageVO {
	private int id;
	private String description;
	private int reportId;
	private String imageId;
	//no report id in constructro
	public ImageVO(int id, String description, String imageId) {
		super();
		this.id = id;
		this.description = description;
		this.imageId = imageId;
	}

	public ImageVO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	
	
	
	
	
	
}
