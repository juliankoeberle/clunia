$( document ).ready(function() {
    loadEvents();
});





function loadEvents(){
	$.get( "/Clunia/api/events", function( data ) {			
		for (var i = 0; i< data.eventVO.length ; i++){
			var arr = data.eventVO[i].date.split(/-|\s|:/);// split string and create array.
			var date = new Date(arr[0], arr[1] , arr[2], arr[3], arr[4], arr[5]); // decrease 
			$("#eventlist").append("<div class='col-sm-12 col-md-12 col-xs-12'>"
				   +"<div class='event'>"
				   		+"<div class='icon'>"
				   			+"<span class='date'>"+date.getDate()+"."+date.getMonth()+"</span>"
				   		+"</div>"
						+"<img class='img' src='"+data.eventVO[i].imageID+"' alt=''>"
						+"<div class='description'>"
							+"<h4><br/>"+data.eventVO[i].name+"</h4>"
							+"<strong>Ort</strong> : "+data.eventVO[i].location+ "<br/>"
							+"<strong>Uhrzeit</strong> "+date.getHours()+":"+date.getMinutes()
						+"</div>"
			       +"</div>"
			+"</div>");
		}
	});
}